package spasvr

import (
	"log"
	"mime"
	"net/http"
	"os"
	"path"
	"strings"

	"gitlab.com/stefarf/nworkers"
)

type empty struct{}

func ServeHTTP(dir string, numberOfWorkers int) func(http.ResponseWriter, *http.Request) {
	q := nworkers.InitWithValueFunction(numberOfWorkers, func() empty {
		return empty{}
	})

	rootIndex := path.Join(dir, "index.html")
	return func(w http.ResponseWriter, r *http.Request) {
		turn := q.Get()
		defer turn.Done()

		filename := path.Join(dir, r.URL.Path)
		statFile(filename, rootIndex,
			// internal error
			func() {
				w.WriteHeader(http.StatusInternalServerError)
			},

			// file exits
			func(filename string) error {
				var contentType string
				if strings.HasSuffix(filename, ".js.map") {
					contentType = "application/json"
				} else {
					idx := strings.LastIndex(filename, ".")
					if idx != -1 {
						contentType = mime.TypeByExtension(filename[idx:])
					}
					if contentType == "" {
						contentType = "text/plain"
					}
				}

				log.Println(contentType, filename)

				w.Header().Set("Content-Type", contentType)
				b, err := os.ReadFile(filename)
				if err != nil {
					return err
				}
				_, err = w.Write(b)
				return err
			},
		)
	}
}

func statFile(
	filename, rootIndex string,
	internalError func(),
	fileExists func(filename string) error,
) {
	info, err := os.Stat(filename)
	if filename == rootIndex {
		if err != nil || info.IsDir() {
			internalError()
			return
		}
		err := fileExists(filename)
		if err != nil {
			internalError()
		}
		return
	}
	if err != nil {
		statFile(rootIndex, rootIndex, internalError, fileExists)
		return
	}
	if info.IsDir() {
		statFile(path.Join(filename, "index.html"), rootIndex, internalError, fileExists)
		return
	}
	err = fileExists(filename)
	if err != nil {
		internalError()
	}
}
