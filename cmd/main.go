package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/stefarf/spasvr"
)

func main() {
	http.HandleFunc("/", spasvr.ServeHTTP(*fDir, *fWorkers))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *fPort), nil))
}
