package main

import (
	"flag"
	"log"
)

var (
	fDir     = flag.String("d", "", "directory of SPA web assets to serve")
	fPort    = flag.Uint("p", 8000, "server port to listen")
	fWorkers = flag.Int("w", 10, "number of workers")
)

func init() {
	flag.Parse()
	if *fDir == "" {
		log.Fatal("-d: invalid value, must be defined")
	}
	if *fWorkers < 1 {
		log.Fatal("-w: invalid value, must be at least 1")
	}
}
